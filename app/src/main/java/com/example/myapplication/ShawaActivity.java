package com.example.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class ShawaActivity extends Activity {
    TextView textView;
    ImageView imageView;
    @Override
    protected void  onCreate(Bundle savedInstanceState){
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_shawa );
        textView = findViewById( R.id.shawatitle );
        imageView = findViewById( R.id.shawaimage );
       textView.setText( getIntent().getStringExtra( "title" ));
       imageView.setImageResource( getIntent().getIntExtra( "image",R.drawable.shawarma2 ) );
    }
}



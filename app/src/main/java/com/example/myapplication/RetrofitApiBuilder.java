package com.example.myapplication;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApiBuilder {
    static public RetrofitInterface  getInterface()

    {
        Retrofit retrofit = new Retrofit.Builder ().
                baseUrl ( "http://eetk.tesan.ru:8089 " )
                .addConverterFactory ( GsonConverterFactory.create () )
                .build ();
        RetrofitInterface retrofitInterface = retrofit.create ( RetrofitInterface.class );
        return retrofitInterface;
    }
}

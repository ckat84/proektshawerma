package com.example.myapplication;

public class AuthResponse {
    Integer userId;
    Integer token;

    public AuthResponse(Integer userId, Integer token) {
        this.userId = userId;
        this.token = token;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }
}

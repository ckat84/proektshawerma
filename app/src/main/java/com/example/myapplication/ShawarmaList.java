package com.example.myapplication;

import java.util.ArrayList;

public class ShawarmaList {
    ArrayList<ShawarmaListItem> shawarma;

    public ArrayList<ShawarmaListItem> getShawarma() {
        return shawarma;
    }

    public void setShawarma(ArrayList<ShawarmaListItem> shawarma) {
        this.shawarma = shawarma;
    }

    public ShawarmaList(ArrayList<ShawarmaListItem> shawarma) {
        this.shawarma = shawarma;

    }
}

package com.example.myapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;

public class ProfileActivity extends AppCompatActivity {
    ImageView imageView;
    private static final int REQUEST_CODE_CAMERA =0;
    private static final int REQUEST_CODE_GALLERY =1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        imageView = findViewById ( R.id.imageView );

    }

    public void emailchangeClick(View view) {
    }

    public void passwordchangeClick(View view) {
    }

    public void exitClick(View view) {
                Intent intent = new Intent(ProfileActivity.this,AuthorizationActivity.class);
        startActivity(intent);
    }

    public void change_foto_Click(View view) {
         Intent cameraIntent =new Intent ( MediaStore.ACTION_IMAGE_CAPTURE );
         startActivityForResult ( cameraIntent,REQUEST_CODE_CAMERA );
             }



    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult ( requestCode,resultCode,data );
        Bitmap tempBitmap;
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK){
            tempBitmap = (Bitmap) data.getExtras ().get ( "data" );
            imageView.setImageBitmap ( tempBitmap );

        }
         if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK);{
            try {
                tempBitmap = MediaStore.Images.Media.getBitmap ( getContentResolver (),data.getData () );
                imageView.setImageBitmap ( Bitmap.createScaledBitmap ( tempBitmap,imageView.getWidth (),imageView.getHeight (),false ) );
            } catch (IOException e) {
                e.printStackTrace ();
            }

        }

    }

    public void change_galeri(View view) {
        Intent galleryIntent = new Intent (Intent.ACTION_PICK);
        galleryIntent.setType ( "image/*" );
        startActivityForResult ( galleryIntent, REQUEST_CODE_GALLERY);
    }
}

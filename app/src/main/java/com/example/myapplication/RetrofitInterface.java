package com.example.myapplication;

import com.example.myapplication.netresponse.RouteResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RetrofitInterface {
      @POST("/maps/api/direction/json")
    Call<RouteResponse> getRoute(
            @Query ( "origin" ) String startplace,
            @Query ( "destination" ) String endplace,
            @Query ( "key" ) String key,
            @Query ( "language" ) String language,
            @Query ( "mode" ) String routemode

    );
        @POST("/pizza/reg")
    Call<UserReg> regUser(
            @Query ( "login" ) String login,
            @Query ( "email" ) String email,
            @Query ( "password" ) String password

        );
        @GET("/pizza")
    Call<ShawarmaList> getShawarmaList();
        @POST("/pizza")
    Call<ShawarmaListItem> getShawarma(
            @Query ( "id" ) Integer id
        );
        @POST("/pizza/auth")
    Call<AuthResponse> autUser(
            @Query ( "login" ) String login,
            @Query("password") String password

        );

};




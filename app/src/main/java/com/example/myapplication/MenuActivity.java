package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity {
RecyclerView recyclerView;
ShawarmaList shawarmaList;
String titles[], descriptions[];
int images[]={R.drawable.shawarma,R.drawable.user,R.drawable.shawarma,R.drawable.user,R.drawable.shawarma, R.drawable.user,
        R.drawable.shawarma,R.drawable.user,R.drawable.shawarma,R.drawable.user,R.drawable.shawarma,R.drawable.user,
        R.drawable.shawarma,R.drawable.user,R.drawable.shawarma,R.drawable.user,R.drawable.shawarma,R.drawable.user,
        R.drawable.shawarma,R.drawable.user};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        RetrofitInterface retrofitInterface = RetrofitApiBuilder.getInterface ();
        Call<ShawarmaList> callShawarma = retrofitInterface.getShawarmaList ();
        callShawarma.enqueue ( new Callback<ShawarmaList> () {
            @Override
            public void onResponse(Call<ShawarmaList> call, Response<ShawarmaList> response) {
                            }

            @Override
            public void onFailure(Call<ShawarmaList> call, Throwable t) {

            }
        } );
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        recyclerView = findViewById( R.id.shawarmalist );
        titles = getResources().getStringArray( R.array.titleshawarma );
        descriptions = getResources().getStringArray( R.array.descriptionshawarma );
        MyRecyclerViewAdapter recyclerViewAdapter  = new MyRecyclerViewAdapter( titles,descriptions,images,this );
        recyclerView.setAdapter( recyclerViewAdapter );
    }

    public void profileClick(View view) {
        Intent intent = new Intent(MenuActivity.this,ProfileActivity.class);
        startActivity(intent);
    }
}

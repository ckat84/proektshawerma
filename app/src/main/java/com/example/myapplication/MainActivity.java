package com.example.myapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    AnimationDrawable animationDrawable;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_main );
        imageView = findViewById ( R.id.imageView );
        animationDrawable = (AnimationDrawable) imageView.getBackground ();
        animationDrawable.setEnterFadeDuration ( 1500 );
        animationDrawable.setEnterFadeDuration ( 2000 );
        animationDrawable.start ();
        new CountDownTimer(3000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(MainActivity.this,AuthorizationActivity.class);
                startActivity(intent);

            }
        }.start();
    }


};



package com.example.myapplication;

public class ShawarmaListItem {
    Integer id;
    String name;
    String description;
    String structure;

    public ShawarmaListItem(Integer id, String name, String description, String structure) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.structure = structure;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }
}

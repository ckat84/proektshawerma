package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthorizationActivity extends AppCompatActivity {

EditText login,password;
RetrofitInterface retrofitInterface;
    SharedPreferences sharedPreferences;
    EditText name;
    CheckBox checkBox;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        login = findViewById(R.id.logineditText);
        password = findViewById(R.id.passwordeditText);
        retrofitInterface = RetrofitApiBuilder.getInterface ();
        checkBox = findViewById ( R.id.checkBox );

    }

    public void loginClick(View view) {
       String mylogin = login.getText().toString();
        String mypassword = password.getText().toString();
        sharedPreferences = getSharedPreferences ( "main",MODE_PRIVATE);

        editor = sharedPreferences.edit ();
        editor.putString ( "login",login.getText ().toString () );
        editor.apply ();

                final Boolean save = sharedPreferences.getBoolean ( "save",false );

        if (save){
            Intent intent = new Intent (AuthorizationActivity.this, MenuActivity.class);
            startActivity ( intent );
        }
        if (checkBox.isChecked ()){
            editor = sharedPreferences.edit ();
            editor = editor.putBoolean ("save",false);
            editor.apply ();
        }
        else {
            editor = sharedPreferences.edit ();
            editor.putBoolean ( "save",false );
            editor.apply ();

        }

        if (!mylogin.equals(" ") && !mypassword.equals(" ")){
            Call<AuthResponse> authCall = retrofitInterface.autUser (mylogin,mypassword);
            authCall.enqueue ( new Callback<AuthResponse> () {
                @Override
                public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                    if (response.isSuccessful ()) {
                        Intent intent = new Intent ( AuthorizationActivity.this, MenuActivity.class );
                        startActivity ( intent );
                                            }


                    else {
                        Toast.makeText ( getApplication (),"Неверный логин или пароль",Toast.LENGTH_LONG ).show ();
                    }
                }

                @Override
                public void onFailure(Call<AuthResponse> call, Throwable t) {
                    Toast.makeText ( getApplication (),"Ошибка сервера",Toast.LENGTH_LONG ).show ();

                }
            } );


                 }
else  {
            Toast.makeText(this,"Поля не должны быть пустыми",Toast.LENGTH_LONG).show();
        }



    }

    public void regClick(View view) {
        Intent intent = new Intent(AuthorizationActivity.this,RegistrationActivity.class);
        startActivity(intent);
    }

    public void anketaregClick(View view) {
    }

    public void backClick(View view) {
    }
}

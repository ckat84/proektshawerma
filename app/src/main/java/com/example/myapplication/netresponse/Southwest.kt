package com.example.myapplication.netresponse

data class Southwest(
    val lat: Double,
    val lng: Double
)
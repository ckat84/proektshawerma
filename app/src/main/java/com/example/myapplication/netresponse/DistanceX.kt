package com.example.myapplication.netresponse

data class DistanceX(
    val text: String,
    val value: Int
)
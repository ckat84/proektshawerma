package com.example.myapplication.netresponse

data class EndLocationX(
    val lat: Double,
    val lng: Double
)
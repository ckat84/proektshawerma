package com.example.myapplication.netresponse

data class Northeast(
    val lat: Double,
    val lng: Double
)
package com.example.myapplication.netresponse

data class EndLocation(
    val lat: Double,
    val lng: Double
)
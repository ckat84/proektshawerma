package com.example.myapplication.netresponse

data class DurationX(
    val text: String,
    val value: Int
)
package com.example.myapplication.netresponse

data class StartLocationX(
    val lat: Double,
    val lng: Double
)
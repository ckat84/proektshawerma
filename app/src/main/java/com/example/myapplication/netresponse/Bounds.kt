package com.example.myapplication.netresponse

data class Bounds(
    val northeast: Northeast,
    val southwest: Southwest
)
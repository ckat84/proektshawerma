package com.example.myapplication.netresponse

data class Duration(
    val text: String,
    val value: Int
)
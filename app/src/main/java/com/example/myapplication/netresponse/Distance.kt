package com.example.myapplication.netresponse

data class Distance(
    val text: String,
    val value: Int
)
package com.example.myapplication.netresponse

data class StartLocation(
    val lat: Double,
    val lng: Double
)
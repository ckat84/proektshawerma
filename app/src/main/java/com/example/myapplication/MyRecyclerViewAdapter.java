package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.MyVeiwHolder> {
String titles[], descriptions[];
int images[];
Context context;
    public MyRecyclerViewAdapter(String[] titles, String[] descriptions, int[] images,Context context) {
        this.titles = titles;
        this.descriptions = descriptions;
        this.images = images;
        this.context = context;
    }

    @NonNull
    @Override
    public MyVeiwHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( context );
        View root = inflater.inflate( R.layout.row_element, parent,false );
        return new MyVeiwHolder (root);
            }

    @Override
    public void onBindViewHolder(@NonNull MyVeiwHolder holder, final int position) {
holder.title.setText( titles[position]);
holder.desc.setText( descriptions[position] );
holder.imageshawarma.setImageResource( images[position] );
holder.linearLayout.setOnClickListener( new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context,ShawaActivity.class);
        intent.putExtra( "title", titles[position]);
        intent.putExtra( "image", images[position] );
        context.startActivity( intent );
    }
} );
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class MyVeiwHolder extends RecyclerView.ViewHolder {
       TextView title, desc;
       ImageView imageshawarma;
       LinearLayout linearLayout;
        public MyVeiwHolder(@NonNull View itemView) {
            super( itemView );
            title = itemView.findViewById(R.id.titleitem );
            desc = itemView.findViewById(R.id.descriptioniten);
            imageshawarma = itemView.findViewById( R.id.shawarmaitem );
            linearLayout = itemView.findViewById( R.id.linearshawarma);
        }
    }
}

package com.example.myapplication;

public class MyMarkerOptions {
    private String drawableName;
    private Double lattitude;
    private Double longitude;

    public MyMarkerOptions(String drawableName, Double lattitude, Double longitude) {
        this.drawableName = drawableName;
        this.lattitude = lattitude;
        this.longitude = longitude;
    }

    public String getDrawableName() {
        return drawableName;
    }

    public void setDrawableName(String drawableName) {
        this.drawableName = drawableName;
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}

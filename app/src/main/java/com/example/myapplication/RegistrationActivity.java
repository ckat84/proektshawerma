package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegistrationActivity extends Activity {
    EditText Name;
    EditText Password;
    EditText RepeatPassword;
    EditText email;
    RetrofitInterface retrofitInterface;
    @Override
protected void onCreate(Bundle savedInstanceStanceState){
    super.onCreate(savedInstanceStanceState);
    setContentView(R.layout.activity_registration);
    Name = findViewById(R.id.NameeditText);
    Password = findViewById(R.id.PasswordeditText);
    RepeatPassword = findViewById(R.id.RepeatPasswordeditText);
    email = findViewById(R.id.emaileditText);
    RetrofitApiBuilder retrofitApiBuilder = new RetrofitApiBuilder ();
    retrofitInterface = retrofitApiBuilder.getInterface ();

}
    public void anketaregClick(View view) {
String myName = Name.getText().toString();
String myPassword = Password.getText().toString();
String myRepeatPassword = RepeatPassword.getText().toString();
String myemal = email.getText().toString();
if(!myName.equals("") && !myPassword.equals("") && !myemal.equals("") && !myRepeatPassword.equals("")){
if(myemal.indexOf("@")>0){
    if (myRepeatPassword.equals(myPassword)){
        Call<UserReg> regCall = retrofitInterface.regUser ( myName,myemal,myPassword );
        regCall.enqueue ( new Callback<UserReg> () {
            @Override
            public void onResponse(Call<UserReg> call, Response<UserReg> response) {

                Toast.makeText ( getApplication (),response.body ().getLogin (),Toast.LENGTH_LONG).show ();

            }

            @Override
            public void onFailure(Call<UserReg> call, Throwable t) {

            }
        } );
        Intent intent = new Intent(RegistrationActivity.this,MenuActivity.class);
       startActivity(intent);
    }



 else{
     Toast.makeText(this,"Повтор пароля введён неверно",Toast.LENGTH_LONG).show();
}
}
 else{
            Toast.makeText(this,"email имеет неверный формат",Toast.LENGTH_LONG).show();
        }
}
 else{
            Toast.makeText(this,"Поля не должны быть пустыми",Toast.LENGTH_LONG).show();
        }



        }

        public void backClick(View view) {
            Intent intent = new Intent(RegistrationActivity.this,AuthorizationActivity.class);
            startActivity(intent);

    }
};